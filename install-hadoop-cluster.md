### 本次安装hadoop的版本为3.2.1
> 安装文件链接：https://pan.baidu.com/s/1qcif-6OiXTyFuWiYEeJcdQ 提取码: p3ss
#### 一、添加hosts主机名映射，三个节点的名称与对应的 IP
在每台机器上都要去添加
```
vi /etc/hosts
```
在文件第一行开始添加如下信息：
```
10.40.0.29  A-BIGDATA-APPTEST #Master
10.40.0.28  A-BIGDATA-DWTEST #slave1
192.168.1.3 A-BIGDATA-DN01 #slave2
```

#### 二、添加hadoop用户
在每台机器上都要去添加
```
groupadd hadoop
useradd -g hadoop -m hadoop -s /bin/bash
passwd hadoop
vi /etc/sudoers
```
在root    ALL=(ALL)       ALL下面添加hadoop  ALL=(ALL)       ALL
```
## Allow root to run any commands anywhere
root    ALL=(ALL)       ALL
hadoop  ALL=(ALL)       ALL
```
#### 三、生成秘钥()
##### 1、在Master机器上执行
```
su - hadoop
cd ~/.ssh               # 如果没有该目录，先执行一次ssh localhost
rm ./id_rsa*            # 删除之前生成的公匙（如果有）
ssh-keygen -t rsa       # 一直按回车就可以

#让 Master节点也能无密码 SSH 本机
cat ./id_rsa.pub >> ./authorized_keys 

#完成后可执行ssh Master验证一下（可能需要输入 yes，成功后执行 exit 返回原来的终端）。
ssh -p 2112 A-BIGDATA-APPTEST 

#在Master节点将上公匙传输到Slave1、Slave2节点
scp ~/.ssh/id_rsa.pub hadoop@A-BIGDATA-DWTEST:/home/hadoop/
scp ~/.ssh/id_rsa.pub hadoop@A-BIGDATA-DN01:/home/hadoop/
```
##### 2、在slave1、slave2机器执行
```
su - hadoop
mkdir .ssh # 如果没有该目录，则新建.ssh目录
cat id_rsa.pub >> ~/.ssh/authorized_keys
rm id_rsa.pub
su - root 
chmod 700 -R /home/hadoop
chmod 644 /home/hadoop/.ssh/authorized_keys 
```
注意$HOME/.ssh目录 或 $HOME目录的权限 最好是700，我就在这里栽跟头了。
注意uthorized_keys的权限 chmod 644 authorized_keys 这个也是要注意的。

#### 四、在Master机器执行验证
```
su - hadoop
ssh -p 2112 A-BIGDATA-DWTEST
ssh -p 2112 A-BIGDATA-DN01
```
#### 五、在Master上安装hadoop
安装 hadoop 之前需要先安装 JDK, 关于 JDK 的安装这里不再赘述。
```
su - root
cd /opt
#解压
sudo tar -zxvf hadoop_3.2.1.tar.gz

#目录重命名
sudo mv hadoop_3.2.1.tar.gz hadoop

#修改用户和用户组
sudo chown —R hadoop:hadoop hadoop/
```
其实解压就好了，因为里面的配置文件都按照上面的目录位置配置好了，下面也讲解下配置文件的各个配置项说明。


#### 六、配置PATH变量
在各个节点包括主节点，执行以下命令
```
su - hadoop
vim ~/.bashrc
```
在末尾添加如下
```
export JAVA_HOME=/opt/java
export JRE_HOME=$JAVA_HOME/jre
export CLASSPATH=.:$JAVA_HOME/lib:$JRE_HOME/lib:$CLASSPATH
export PATH=$JAVA_HOME/bin:$JRE_HOME/bin:$PATH
#Add HADOOP_HOME PATH
export HADOOP_HOME=/opt/hadoop
export PATH=$PATH:$HADOOP_HOME/bin:$HADOOP_HOME/sbin
```
保存后执行如下命令，使配置生效。
```
source ~/.bashrc 
```
#### 七、配置集群环境
集群/分布式模式需要修改 /opt/hadoop/etc/hadoop 中的5个配置文件，更多设置项可点击查看官方说明，这里仅设置了正常启动所必须的设置项： workers(3.2.1之前的版本是slaves)、core-site.xml、hdfs-site.xml、mapred-site.xml、yarn-site.xml 。

##### 1、文件 workers
将作为 DataNode 的主机名写入该文件，每行一个，默认为 localhost，所以在伪分布式配置时，节点即作为 NameNode 也作为 DataNode。分布式配置可以保留 localhost，也可以删掉，让 Master 节点仅作为 NameNode 使用。本教程让Master节点仅作为 NameNode 使用，因此将文件中原来的 localhost 删除，只添加一行内容：Slave1和Slave2。
```
vi workers
```
添加如下内容
```
A-BIGDATA-DWTEST
A-BIGDATA-DN01
~
```
##### 2、文件core-site.xml 改为下面的配置：
```xml
<configuration>
    <property>
        <name>hadoop.tmp.dir</name>
        <value>file:/home/hadoop/tmp</value>
        <description>Abase for other temporary directories.</description>
    </property>
    <property>
        <name>fs.defaultFS</name>
        <value>hdfs://A-BIGDATA-APPTEST:9000</value>
    </property>
</configuration>
```
##### 3、文件 hdfs-site.xml，dfs.replication 一般设为 3，但我们有2个 Slave 节点，所以 dfs.replication 的值还是设为2：
```xml
<configuration>
    <property>
        <name>dfs.namenode.secondary.http-address</name>
        <value>A-BIGDATA-APPTEST:50090</value>
    </property>
    <property>
        <name>dfs.replication</name>
        <value>2</value>
    </property>
    <property>
        <name>dfs.namenode.name.dir</name>
        <value>file:/home/hadoop/tmp/dfs/name</value>
    </property>
    <property>
        <name>dfs.datanode.data.dir</name>
        <value>file:/home/hadoop/tmp/dfs/data</value>
    </property>
</configuration>
```
##### 4、文件 mapred-site.xml （可能需要先重命名，默认文件名为 mapred-site.xml.template），然后配置修改如下：
```xml
<configuration>
        <property>
                <name>mapreduce.framework.name</name>
                <value>yarn</value>
        </property>
        <property>
                <name>mapreduce.jobhistory.address</name>
                <value>A-BIGDATA-APPTEST:10020</value>
        </property>
        <property>
                <name>mapreduce.jobhistory.webapp.address</name>
                <value>A-BIGDATA-APPTEST:19888</value>
        </property>
        <property>
                <name>mapreduce.admin.user.env</name>
                <value>HADOOP_MAPRED_HOME=${HADOOP_HOME}</value>
        </property>
        <property>
                <name>yarn.app.mapreduce.am.env</name>
                <value>HADOOP_MAPRED_HOME=${HADOOP_HOME}</value>
        </property>
</configuration>
```
##### 5、文件 yarn-site.xml：
```xml
<configuration>
        <property>
                <name>yarn.resourcemanager.hostname</name>
                <value>A-BIGDATA-APPTEST</value>
        </property>
        <property>
                <name>yarn.nodemanager.aux-services</name>
                <value>mapreduce_shuffle</value>
        </property>
</configuration>
```
配置好后，将Master上的 /opt/Hadoop 文件夹复制到各个节点上。

在Master节点上执行：
```bash
su - root
cd /opt
tar -zcvf hadoop.master.tar.gz hadoop   # 先压缩再复制
mv hadoop.master.tar.gz /home/hadoop
chown -R hadoop:hadoop /home/hadoop
su - hadoop
scp hadoop.master.tar.gz A-BIGDATA-DWTEST:/home/hadoop
scp hadoop.master.tar.gz A-BIGDATA-DN01:/home/hadoop
```
在 Slave1 节点上执行：
```
su - root
sudo tar -zxvf /home/hadoop/hadoop.master.tar.gz -C /opt/
sudo chown -R hadoop /usr/local/hadoop
```
同样，如果有其他 Slave 节点，也要执行将 hadoop.master.tar.gz 传输到 Slave 节点、在 Slave 节点解压文件的操作。

分布式安装：http://dblab.xmu.edu.cn/blog/install-hadoop-cluster/

#### 八、格式化
首次启动需要先在 Master 节点执行 NameNode 的格式化：
```
hdfs namenode -format  #首次运行需要执行初始化，之后不需要
```
#### 九、启动hadoop
启动需要在 Master 节点上进行
```
start-dfs.sh
start-yarn.sh
mapred --daemon start historyserver
mr-jobhistory-daemon.sh start historyserver
```
#### 十、关闭hadoop
启动需要在 Master 节点上进行
```
stop-dfs.sh
stop-yarn.sh
mapred --daemon stop historyserver
```
#### 十一、添加新节点
##### 1.说明
> Hadoop集群已经运行正常，现在新买了一些机子，要加入到集群里面增加新的节点。以下就是增加的过程。
##### 2.配置运行环境
- 1、安装与master和其他slave相同的java环境，jdk版本要相同。具体安装过程这里不再赘述。
- 2、修改新节点的ip和主机名对应关系，修改/etc/hosts配置文件，定义ip与hostname的映射。
- 3、创建hadoop用户，具体操作如上面的第二点，关闭新节点防火墙，因为Hadoop集群是在内网环境运行，可以关闭防火墙。
- 配置新节点ssh免密码登录，使得master可以免密码登录到新节点主机。具体操作如上面的第三点。
##### 3.修改集群所有节点相关配置文件
- 1、修改集群所有节点（master和所有slave）的${HADOOP_HOME}/etc/hadoop/workers文件，增加新节点ip或hostname：
- 2、同时，集群所有节点上修改系统的/etc/hosts配置文件，增加ip与hostname的映射:新节点ip 新节点主机名

##### 4.新节点增加Hadoop
将master（namenode）中的配置拷贝到新节点（datanode），而且新节点的Hadoop的目录路径最好是和集群的其他节点一样，便于查看和管理。这一步只要用scp拷贝即可。
切换到Hadoop用户名，然后拷贝：
```
su - hadoop 
scp -r /hadoop安装目录 新节点上的Hadoop用户名@新节点主机ip或主机名
```
这一步完成就说明新节点的安装完毕。
##### 5.单独启动新节点
集群增加了新节点之后，不需要关闭整个集群然后重启。可以单独启动新增加的节点，实现新节点动态加入。在新节点上执行命令：
```
[jay@slave2 ~]# cd /opt
[jay@slave2 hadoop-1.2.1]# ./bin/hadoop-daemon.sh start datanode #启动datanode
starting datanode...
[jay@slave2 hadoop-1.2.1]# ./bin/hadoop-daemon.sh start tasktracker #启动tasktracker
starting tasktracker...
```
##### 6.验证启动成功
###### 5.1访问web UI查看集群信息
访问http://master:9870/可以看到"Live Nodes"的数量，如果增加了新节点数，说明添加成功。
###### 5.2 执行命令查看
$ bin/hadoop dfsadmin -report 

##### 7.集群负载均衡
在master主机上面，运行start-balancer.sh进行数据负载均衡。目的是将其他节点的数据分担一些到新节点上来，看集群原来数据的多少需要一定的时间才能完成。
```
hadoop balancer
```
或者
```
bin/start-balancer.sh
```
负载均衡作用：当节点出现敀障，或新增加节点时，数据块可能分布不均匀，负载均衡可重新平衡各个datanode上数据块的分布，使得所有的节点数据和负载能处于一个相对平均的状态，从而避免由于新节点的加入而效率降低(如果不进行balance,新数据一般会被插入到新节点中)
