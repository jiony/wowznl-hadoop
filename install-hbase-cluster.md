### 本次安装hbase的版本为2.2.6
> 安装文件链接：https://pan.baidu.com/s/1TRK_ChCZgLhWBaj5pPupeg 提取码: 7s9j

安装hbase之前需要基于：install-hadoop-cluster.md 此文档先安装好hadoop,以及install-zookeeper-cluster.md 此文档先安装好zookeeper
#### 一、环境
```
Linux：Centos Linux 7.2
JDK：jdk1.8.0_212
Hadoop：3.2.1
Zookeeper：3.6.2
HBase：2.2.6
```
#### 二、服务器
```
10.40.0.29  A-BIGDATA-APPTEST #Master
10.40.0.28  A-BIGDATA-DWTEST #slave1
192.168.1.3 A-BIGDATA-DN01 #slave2
```
#### 三、安装步骤
##### 1、下载hbase安装包hbase-1.4.8-bin.tar.gz，并上传到bigdata01服务器的/opt目录中。
##### 2、解压安装包，
```bash
tar -zvxf hbase-1.4.8-bin.tar.gz

#然后将目录hbase-1.4.8修改为hbase，
mv hbase-1.4.8 hbase
```

##### 3、编辑hbase-env.sh文件
```
vi /opt/hbase/conf/hbase-env.sh 
```
修改JAVA_HOME
```
export JAVA_HOME=/opt/java
```
注释掉下面两行
```
# export HBASE_MASTER_OPTS="$HBASE_MASTER_OPTS -XX:PermSize=128m -XX:MaxPermSize=128m -XX:ReservedCodeCacheSize=256m"
# export HBASE_REGIONSERVER_OPTS="$HBASE_REGIONSERVER_OPTS -XX:PermSize=128m -XX:MaxPermSize=128m -XX:ReservedCodeCacheSize=256m"
```
HBASE_MANAGES_ZK变量，此变量默认为true，告诉HBase是否启动/停止ZooKeeper集合服务器作为HBase启动/停止的一部分。如果为true，这Hbase把zookeeper启动，停止作为自身启动和停止的一部分。如果设置为false，则表示独立的Zookeeper管理。我们将其设置为false.
```
export HBASE_MANAGES_ZK=false
```
修改Hbase堆设置，将其设置成4G，
```
export HBASE_HEAPSIZE=4G
```
若ssh默认端口不是22端口，则需要修改如下配置，增加-p 2112配置。
```
export HBASE_SSH_OPTS="-p 2112 -o ConnectTimeout=1 -o SendEnv=HBASE_CONF_DIR"
```

##### 4、编辑配置文件hbase-site.xml
```xml
<?xml version="1.0"?>
<?xml-stylesheet type="text/xsl" href="configuration.xsl"?>
<configuration>
	<property>
		<name>hbase.zookeeper.quorum</name>
	    <value>A-BIGDATA-APPTEST:2181,A-BIGDATA-DN01:2181,A-BIGDATA-DWTEST:2181</value>
	</property>	
  	<property>
    	<name>hbase.rootdir</name>
    	<value>hdfs://A-BIGDATA-APPTEST:9000/hbase</value>
  	</property>
  	<property>
    	<name>hbase.cluster.distributed</name>
    	<value>true</value>
  	</property>
  	<property>
        <name>hbase.tmp.dir</name>
        <value>./tmp</value>
    </property>
    <property>
        <name>hbase.unsafe.stream.capability.enforce</name>
        <value>false</value>
    </property>
</configuration>
```
> hbase.zookeeper.quorum： 这个参数是用来设置zookeeper服务列表，每个服务器之间使用使用逗号分隔，2181是zookeeper默认端口号，你可以自行根据你的端口号添加，默认的端口号加不加都无所谓。

> hbase.rootdir: 这个参数是用来设置RegionServer 的共享目录，用来存放HBase数据。特别需要注意的是 hbase.rootdir 里面的 HDFS 地址是要跟 Hadoop 的 core-site.xml 里面的 fs.defaultFS 的 HDFS 的 IP 地址或者域名、端口必须一致。

> hbase.cluster.distributed： HBase 的运行模式。为 false 表示单机模式，为 true 表示分布式模式。
##### 5、编辑regionservers文件
配置从服务器，去掉 localhost，添加 slaves 节点
```
A-BIGDATA-DN01
A-BIGDATA-DWTEST
```

##### 6，把 hadoop 的 hdfs-site.xml 复制一份到 hbase 的 conf 目录下
```
cp /opt/hadoop/etc/hadoop/hdfs-site.xml /opt/hbase/conf/
```

##### 7、将配置好的 habase 分发到其它节点对应的路径下
```
scp -r /opt/hbase root@bigdata02:/opt/
scp -r /opt/hbase root@bigdata03:/opt/
```

##### 8、分别在三台服务器上增加HBASE_HOME环境变量，修改vim /etc/profile
```
export HBASE_HOME=/opt/hbase
export PATH=$HBASE_HOME/bin:$PATH
```
使配置生效
```
source /etc/profile
```

##### 9，同步服务器时间
我们在使用HDFS的时候经常会出现一些莫名奇妙的问题，通常可能是由于多台服务器的时间不同步造成的。我们可以使用网络时间服务器进行同步。
安装ntpdate工具
```
yum -y install ntp ntpdate
```
设置系统时间与网络时间同步
```
ntpdate cn.pool.ntp.org
```
将系统时间写入硬件时间
```
hwclock --systohc
```
查看系统时间
```
timedatectl
```
##### 10、启动hbase
在 Master上输入
```
start-hbase.sh，
```
##### 11、关闭hbase
在 Master上输入
```
stop-hbase.sh，
```
通过浏览器访问http://A-BIGDATA-APPTEST:16010可以查看hbase 集群状态。