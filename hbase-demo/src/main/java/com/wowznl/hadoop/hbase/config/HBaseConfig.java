package com.wowznl.hadoop.hbase.config;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.hadoop.hbase.HbaseTemplate;

/**
 * 把配置加入到hbaseTemplate中，交给spring容器
 * 再把Admin交给spring容器
 */
@Configuration
@EnableConfigurationProperties(HBaseProperties.class)
public class HBaseConfig {


    private final HBaseProperties properties;

    public HBaseConfig(HBaseProperties properties){
        this.properties =properties;
    }

    @Bean
    public HbaseTemplate hbaseTemplate(){
        HbaseTemplate hbaseTemplate =new HbaseTemplate();
        hbaseTemplate.setConfiguration(configuration());
        hbaseTemplate.setAutoFlush(true);
        hbaseTemplate.setEncoding("UTF-8");
        return hbaseTemplate;
    }

    @Bean
    public Admin hbaseAdmin() throws IOException {
    	Connection con=ConnectionFactory.createConnection(configuration());
    	return con.getAdmin();
    }

    private org.apache.hadoop.conf.Configuration configuration() {
        org.apache.hadoop.conf.Configuration configuration = HBaseConfiguration.create();
        Map<String, String> config = properties.getConfig();
        Set<String> keySet = config.keySet();
        for (String key : keySet) {
            configuration.set(key,config.get(key));
        }
        return configuration;
    }
}
