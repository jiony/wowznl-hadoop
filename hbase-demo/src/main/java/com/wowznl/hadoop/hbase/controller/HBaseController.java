package com.wowznl.hadoop.hbase.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wowznl.hadoop.hbase.util.HBaseUtil;

@RestController
@RequestMapping("/hbase")
public class HBaseController {

    @Autowired
    private HBaseUtil hBaseUtil;

    @RequestMapping("/findAll")
    public String findAll(){
        String user = hBaseUtil.findAll("Student");
        System.out.println(user);
        return user;
    }
}
