package com.wowznl.hadoop.hbase.test;


import java.io.IOException;

import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.wowznl.hadoop.hbase.util.HBaseUtil;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class Test01 {

    @Autowired
    private HBaseUtil hbaseUtil;

    /**
     * 创建表
     * @throws IOException
     */
    //@Test
    public void createTable() throws IOException {
        HTableDescriptor desc=new HTableDescriptor(TableName.valueOf("Student"));
        desc.addFamily(new HColumnDescriptor("info".getBytes()));
        String tableName = "Student";
        String familyName = "info";
        hbaseUtil.createTable(tableName, familyName);
    }


    /**
     * 添加数据
     */
    @Test
    public void insert()  {
        hbaseUtil.insert("Student","row1","info","subject","数学");
        hbaseUtil.insert("Student","row1","info","score","99");
        hbaseUtil.insert("Student","row1","info","name","钟东华");
    }



    /**
     * 查询所有数据
     */
    @Test
    public void findAll(){
    	String json = hbaseUtil.findAll("Student");
        System.out.println(json);
    }




    /**
     * 添加列,族(family)
     * @throws IOException
     */
    //@Test
    public void addFamily() throws IOException {
    	String[] family = {"abc"};
    	hbaseUtil.addFamily("Student", family);
    }

}
